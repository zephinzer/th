package main

import (
	"github.com/spf13/cobra"
)

var (
	rootCommand *cobra.Command
)

func main() {
	rootCommand.Execute()
}

func init() {
	initConfig()
	rootCommand = &cobra.Command{
		Use: "th",
		Run: func(command *cobra.Command, args []string) {
			command.Help()
		},
	}
	addCommandsTo(rootCommand)
	addFlagsTo(rootCommand)
}
