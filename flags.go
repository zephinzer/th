package main

import (
	"github.com/spf13/cobra"
)

func addFlagsTo(command *cobra.Command) {
	command.PersistentFlags().BoolVarP(&rootConfig.Daemonize, "daemonize", "d", false, "indicates whether we should run in background mode")
}
