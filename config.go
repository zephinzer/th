package main

type Configuration struct {
	Daemonize bool `json:"daemonize"`
}

var rootConfig *Configuration

func initConfig() {
	rootConfig = &Configuration{}
}
