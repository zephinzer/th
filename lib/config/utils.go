package config

import (
	"os"

	"github.com/spf13/viper"
)

func ReadYAMLConfigurationInto(this interface{}, defaults map[string]interface{}) {
	config := viper.New()

	for key, value := range defaults {
		config.SetDefault(key, value)
	}
	config.SetConfigName(".teamhero")
	config.SetConfigType("yaml")
	cwdir, err := os.Getwd()
	if err == nil {
		config.AddConfigPath(cwdir)
	}
	homedir, err := os.UserHomeDir()
	if err == nil {
		config.AddConfigPath(homedir)
	}
	err = config.ReadInConfig()
	if err != nil {
		panic(err)
	}
	err = config.Unmarshal(&this)
	if err != nil {
		panic(err)
	}
}
