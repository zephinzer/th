package logger

import (
	thelogger "github.com/usvc/logger"
)

func Create() thelogger.Logger {
	return thelogger.New(thelogger.Config{
		Format: thelogger.FormatText,
		Level:  thelogger.LevelTrace,
	})
}
