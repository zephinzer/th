package start

import (
	"github.com/zephinzer/teamhero/lib/config"
)

type VPNConfiguration struct {
	Name          string `yaml:"name"`
	PingAddress   string `yaml:"pingAddress"`
	PingFrequency int    `yaml:"pingFrequency"`
}

type Configuration struct {
	BindAddress string             `yaml:"bindAddress"`
	Networks    []VPNConfiguration `yaml:"networks"`
}

func NewConfig() *Configuration {
	var conf Configuration
	config.ReadYAMLConfigurationInto(&conf, map[string]interface{}{
		"bindAddress": "0.0.0.0:57326",
	})
	return &conf
}
