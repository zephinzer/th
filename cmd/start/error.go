package start

import "time"

type Error struct {
	Type      string
	Name      string
	Error     error
	Timestamp time.Time
}

func NewError(errorType string, errorName string, errorData error) *Error {
	return &Error{errorType, errorName, errorData, time.Now()}
}
