package start

import (
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"
)

type Check func(*Configuration) *Error

func GetChecks(config *Configuration, errors chan *Error) []func() {
	checks := []func(){
		WrapCheck(checkVPNStatus, config, errors),
	}
	return checks
}

func WrapCheck(check Check, config *Configuration, errors chan *Error) func() {
	return func() {
		errors <- check(config)
		tick := time.Tick(5 * time.Second)
		for {
			select {
			case <-tick:
				errors <- check(config)
			}
		}
	}
}

func checkVPNStatus(config *Configuration) *Error {
	errorString := ""
	for i := 0; i < len(config.Networks); i++ {
		vpn := config.Networks[i]
		req, err := http.NewRequest("GET", vpn.PingAddress, nil)
		if err != nil {
			errorString += fmt.Sprintf("[%s - %s: %s], ", vpn.Name, vpn.PingAddress, err.Error())
			continue
		}
		req.Header.Add("User-Agent", "TeamHero")
		client := http.Client{}
		log.Tracef("[network:%s] pinging '%s'...", vpn.Name, vpn.PingAddress)
		res, err := client.Do(req)
		if err != nil {
			errorString += fmt.Sprintf("[%s - %s: %s], ", vpn.Name, vpn.PingAddress, err.Error())
			continue
		}
		if res.StatusCode != http.StatusOK {
			errorString += fmt.Sprintf("[%s - %s > %v], ", vpn.Name, vpn.PingAddress, res.StatusCode)
			continue
		}
		log.Debugf("[network:%s] ping to '%s' was successful", vpn.Name, vpn.PingAddress)
	}
	if len(errorString) == 0 {
		return NewError("network", "failed connectivity", nil)
	}
	return NewError("network", "failed connectivity", errors.New(strings.Trim(errorString, ", ")))
}
