package start

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/0xAX/notificator"
	"github.com/gorilla/mux"
	"github.com/spf13/cobra"
	"github.com/zephinzer/teamhero/lib/logger"
)

var log = logger.Create()

func GetCommand() *cobra.Command {
	config := NewConfig()
	command := &cobra.Command{
		Use: "start",
		Run: Runner(config),
	}
	return command
}

func Runner(config *Configuration) func(*cobra.Command, []string) {
	return func(command *cobra.Command, args []string) {
		returnedErrors := map[string]*Error{}
		errors := make(chan *Error, 64)
		checks := GetChecks(config, errors)
		done := make(chan bool)
		// start the checks
		for _, check := range checks {
			go check()
		}
		// process the errors
		go func() {
			for {
				select {
				case err := <-errors:
					if err != nil {
						errorKey := fmt.Sprintf("%s - %s", err.Type, err.Name)
						if err.Error != nil {
							if returnedErrors[errorKey] == nil {
								message := fmt.Sprintf("%s failed: %s", errorKey, err.Error.Error())
								log.Warn(message)
								notificator.New(notificator.Options{
									AppName: "TeamHero",
								}).Push(errorKey, err.Error.Error(), "", notificator.UR_CRITICAL)
							}
							returnedErrors[errorKey] = err
						} else {
							if returnedErrors[errorKey] != nil {
								message := fmt.Sprintf("%s recovered", errorKey)
								log.Info(message)
								notificator.New(notificator.Options{
									AppName: "TeamHero",
								}).Push(errorKey, message, "", notificator.UR_NORMAL)
							}
							delete(returnedErrors, errorKey)
						}
					}
				}
			}
		}()

		router := mux.NewRouter()

		router.
			Name("apex").
			Path("/").
			Handler(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				log.Infof("%s %s %s%s %s %s", r.Method, r.Proto, r.Host, r.URL.RawPath, r.RemoteAddr, r.Header.Get("User-Agent"))
				errorCount := 0
				for range returnedErrors {
					errorCount++
				}
				errorJSON, err := json.Marshal(returnedErrors)
				if err != nil {
					w.Write([]byte(err.Error()))
					return
				}
				if errorCount > 0 {
					w.WriteHeader(http.StatusInternalServerError)
				}
				w.Write(errorJSON)
			}))

		server := http.Server{
			Addr:    config.BindAddress,
			Handler: router,
		}
		log.Infof("starting server at %s", config.BindAddress)
		done <- server.ListenAndServe() == nil
		<-done
	}
}
