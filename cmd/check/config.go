package check

import "github.com/zephinzer/teamhero/lib/config"

type SoftwareRequirement struct {
	Name          string   `yaml:"name"`
	Invocation    string   `yaml:"invocation"`
	Description   string   `yaml:"description"`
	InstallScript []string `yaml:"installScript"`
}

type Configuration struct {
	Software []SoftwareRequirement `yaml:"software"`
}

func NewConfig() *Configuration {
	var conf Configuration
	config.ReadYAMLConfigurationInto(&conf, map[string]interface{}{})
	return &conf
}
