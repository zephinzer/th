package check

import (
	"os/exec"

	"github.com/spf13/cobra"
	"github.com/zephinzer/teamhero/lib/logger"
)

var log = logger.Create()

func GetCommand() *cobra.Command {
	config := NewConfig()
	command := &cobra.Command{
		Use: "check",
		Run: Runner(config),
	}
	return command
}

func Runner(config *Configuration) func(*cobra.Command, []string) {
	return func(command *cobra.Command, args []string) {
		for _, software := range config.Software {
			log.Debugf("checking for %s ('%s')...", software.Name, software.Invocation)
			softwarePath, err := exec.LookPath(software.Invocation)
			if err != nil {
				log.Warnf("%s binary - `%s` - was not found %s", software.Name, software.Invocation, err)
				if len(software.Description) > 0 {
					log.Infof("%s: %s", software.Name, software.Description)
				}
				continue
			}
			log.Infof("%s binary - `%s` - found at %s", software.Name, software.Invocation, softwarePath)
		}
	}
}
