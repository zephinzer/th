package main

import (
	"github.com/spf13/cobra"
	"github.com/zephinzer/teamhero/cmd/check"
	"github.com/zephinzer/teamhero/cmd/start"
)

func addCommandsTo(command *cobra.Command) {
	command.AddCommand(start.GetCommand())
	command.AddCommand(check.GetCommand())
}
